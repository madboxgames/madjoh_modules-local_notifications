define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/wording/wording'
],
function (require, CustomEvents, Wording){
	var LocalNotifications = {
		PERMISSION_GRANTED 	: 1,
		PERMISSION_REJECTED : 2,

		init : function(settings){
			LocalNotifications.map = settings;

			LocalNotifications.scheduledKeys = localStorage.getItem('com.madjoh.notifications.local');
			if(!LocalNotifications.scheduledKeys || true) LocalNotifications.scheduledKeys = [];

			if(!window.cordova) return;

			cordova.plugins.notification.local.on('click', function(notification){
				var data = JSON.parse(notification.data);
				var notificationData = LocalNotifications.map[data.key];

				var options = notification.data.options || {};
					options.key 	= notificationData.targetPageKey;
					options.stack 	= true;

				CustomEvents.fireCustomEvent(document, 'ChangePage', options);

				cordova.plugins.notification.local.clearAll();
			});

			CustomEvents.addCustomEventListener(document, 'LoggedIn', function(){
				LocalNotifications.getPermissions().then(function(){
					LocalNotifications.scheduleAll();
				});
			});
			CustomEvents.addCustomEventListener(document, 'LoggingOut', function(){
				LocalNotifications.cancelAll();
			});

			CustomEvents.addCustomEventListener(document, 'Notification', function(types){
				if(types.challenges_received) LocalNotifications.scheduleOne('challenges_pending');
				if(types.achievement_completion) LocalNotifications.postponeOne('achievement_completion');
			});
		},
		getPermissions : function(){
			return new Promise(function(resolve, reject){
				if(!window.cordova) return reject();

				cordova.plugins.notification.local.hasPermission(function(granted){
					if(granted){
						localStorage.setItem('com.madjoh.notification.permission', LocalNotifications.PERMISSION_GRANTED);
						return resolve();
					}

					var permission = localStorage.getItem('com.madjoh.notification.permission');
					if(permission === LocalNotifications.PERMISSION_REJECTED) return reject();

					cordova.plugins.notification.local.registerPermission(function(granted){
						if(granted){
							localStorage.setItem('com.madjoh.notification.permission', LocalNotifications.PERMISSION_GRANTED);
							return resolve();
						}

						localStorage.setItem('com.madjoh.notification.permission', LocalNotifications.PERMISSION_REJECTED);
						return reject();
					});
				});
			});
		},
		scheduleOne : function(notificationKey){
			if(!window.cordova) return;
			if(LocalNotifications.scheduledKeys.indexOf(notificationKey) > -1) return;

			var notification = LocalNotifications.map[notificationKey];
			if(!notification.active) return;
			var promise = notification.condition ? notification.condition() : Promise.resolve();

			promise.then(function(options){
				if(!options) options = {};

				var notifications = [];
				for(var i = 0; i < notification.days.length; i++){
					var textKeyIndex = Math.min(i, notification.textKeys.length - 1);
					var textKey = notification.textKeys[textKeyIndex];

					var now = new Date();
					var minutes = Math.floor(Math.random() * 60);
					var date = new Date(now.getTime() + notification.days[i] * 1000 * 3600 * 24);
					// var date = new Date(now.getTime() + notification.days[i] * 1000 * 10);
						date.setHours(11); date.setMinutes(minutes); date.setSeconds(0); date.setMilliseconds(0);

					notifications.push({
						id		: notification.code * 10 + i,
						title	: Wording.getText('justdare'),
						text	: Wording.getText(textKey, options),
						at		: date,
						data 	: {key : notificationKey, options : options}
					});
				}

				cordova.plugins.notification.local.schedule(notifications);

				LocalNotifications.scheduledKeys.push(notificationKey);
				localStorage.setItem('com.madjoh.notifications.local', LocalNotifications.scheduledKeys);
			});
		},
		scheduleAll : function(){
			for(var notificationKey in LocalNotifications.map) LocalNotifications.scheduleOne(notificationKey);
		},
		cancelOne : function(notificationKey){
			if(!window.cordova) return;

			var index = LocalNotifications.scheduledKeys.indexOf(notificationKey);
			if(index === -1) return;

			var notification = LocalNotifications.map[notificationKey];
			for(var i = 0; i < notification.days.length; i++){
				cordova.plugins.notification.local.cancel(notification.code * 10 + i);	
			}

			LocalNotifications.scheduledKeys.splice(index, 1);
			localStorage.setItem('com.madjoh.notifications.local', LocalNotifications.scheduledKeys);
		},
		cancelAll : function(){
			for(var notificationKey in LocalNotifications.map) LocalNotifications.cancelOne(notificationKey);
		},
		postponeOne : function(notificationKey){
			LocalNotifications.cancelOne(notificationKey);
			LocalNotifications.scheduleOne(notificationKey);
		},
		postponeAll : function(){
			LocalNotifications.cancelAll();
			LocalNotifications.scheduleAll();
		}
	};

	return LocalNotifications;
});
